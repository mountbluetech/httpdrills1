const http = require("http");
const crypto = require("crypto");

let server = http.createServer((request, response) => {
  if (request.method == "GET") {
    if (request.url === "/html") {
      response.statusCode = 200;
      response.setHeader("Content-Type", "Text/html");
      response.write(`<!DOCTYPE html>
     <html>
       <head>
       </head>
       <body>
           <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
           <p> - Martin Fowler</p>
     
       </body>
     </html>`);
      response.end();
    } else if (request.url === "/json") {
      response.statusCode = 200;
      response.setHeader("Content-Type", "application/json");
      response.write(
        JSON.stringify({
          slideshow: {
            author: "Yours Truly",
            date: "date of publication",
            slides: [
              {
                title: "Wake up to WonderWidgets!",
                type: "all",
              },
              {
                items: [
                  "Why <em>WonderWidgets</em> are great",
                  "Who <em>buys</em> WonderWidgets",
                ],
                title: "Overview",
                type: "all",
              },
            ],
            title: "Sample Slide Show",
          },
        })
      );
      response.end();
    } else if (request.url === "/uuid") {
      let uuid = crypto.randomUUID();
      response.statusCode = 200;
      response.setHeader("Content-Type", "application/json");
      response.write(
        JSON.stringify({
          uuid: `${uuid}`,
        })
      );
      response.end();
    } else if (request.url.substring(0, 8) === "/status/") {
      let code = Number(request.url.substring(8));
      response.writeHead(code, { "Content-Type": "text/plain" });
      response.end(`Responded with ${code} status code`);
    } else if (request.url.substring(0, 7) === "/delay/") {
      let seconds = Number(request.url.substring(7));
      setTimeout(
        () => response.end(`Responding after ${seconds} seconds`),
        1000 * seconds
      );
    }
  }
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
